import Post from '../Post/Post'
import './posts.css'

export default function Posts() {
    return (
        <div>
            <div className="posts">
                <Post />
                <Post />
                <Post />
                <Post />
                <Post />  
                <Post />
            </div>
        </div>
    )
}
