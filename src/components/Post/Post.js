import './post.css';

export default function Post() {
    return (
        <div className="post">
            <img className="postImg" src="../Assets/077.jpg" alt=""/>
            <div className="postInfo">
                <div className="postCats">
                    <div className="postCat">MObileAPP</div>
                    <div className="postCat">ReactNative</div>
                </div>
                <span className="postTitle">Mobile Apps</span>
               
            </div>      
        </div>
    )
}
