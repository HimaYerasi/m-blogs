
import React, { useEffect, useState } from "react";
import { Editor } from "react-draft-wysiwyg";
import { EditorState } from "draft-js";
import "./write.css"
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
export default function Write() {
    const [editorState, setEditorState] = useState(() =>
        EditorState.createEmpty()
    );
    useEffect(() => {
        console.log(editorState);
    }, [editorState]);
    return (
        <div className="write">
            <form clasName="writeForm">
                <div className="writeFormGroup">
                    <input type="text" placeholder="Title" className="writeTitle" autoFocus={true} />


                    <div style={{ border: "1px solid black", width: " 80% ", minHeight: '400px' }}>
                        <Editor placeholder="Write a Blog"
                            editorState={editorState}
                            onEditorStateChange={setEditorState}
                        />
                    </div>
                    <label htmlFor="fileInput"><i class="fas fa-plus"></i></label>
                    <input type="file" id="fileInput" className="fileUpload" />


                </div>
                <button className="writeSubmit">Publish</button>
            </form>
        </div>
    );
}