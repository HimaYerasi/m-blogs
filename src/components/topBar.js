import React from 'react';
import { Link } from 'react-router-dom';
import "./topbar.css";


export default function TopBar() {
    return (

        <div className="top">

            <div className="topleft">
                <div className="search-container">
                    <input type="text" placeholder="Search.." name="search" />
                    <button type="submit"><i class="fa fa-search"></i></button>
                </div>
            </div>
            <div className="topright">
            <Link to="/Home">  <button className="button button2">Home</button></Link>
                <Link to="/Write">  <button className="button button2">Write a blog</button></Link>
            </div>
        </div>

    )
}

