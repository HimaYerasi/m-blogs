import "./home.css";
import Posts from "../Posts/Posts";
import TopBar from "../topBar";


export default function Home() {
    return (
        <div>
            <TopBar />
            <Posts />
        </div>
    )
}
