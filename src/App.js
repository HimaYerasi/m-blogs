import React from "react";
import TopBar from "./components/topBar";
import Home from "./components/Home/Home";
import Write from "./components/write";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";


function App() {
  return (
    <Router>
   
      <Switch>
        <Route exact path="/">
          <Home />
        </Route>
        <Route path="/Write">
          <Write />
        </Route>
      </Switch>

    </Router>
  );
}

export default App;
